Feature: Add Todo
  Scenario: Create todo by POST /todos
    Given ensure rest endpoint of "35.238.130.67:9010" is up
    When a POST request to /todos is made
    And the request body is
      """
{
  "targetDate": "2020-03-03",
  "description": "Test",
  "user": "Guru17",
  "done": false
}
      """
    Then a 201 response is returned within 2000ms